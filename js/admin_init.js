$(document).ready(function () {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if ($(e.target).attr('has-opened') != '1') {
            $(e.target).attr('has-opened', '1');
            if (target == '#stands') {
                initStandTable();
            }
            else if (target == '#bikes') {
                initBikeTable();
            }
            else if (target == '#users') {
                initUserTable();
            }
            else if (target == '#inquiries') {
                $("#inquiries-data").addClass("report-active");
                $("#inquiry_list_tab").show();
                initInquiryTable();
                initHelpTable();
            }
            else if (target == '#videos') {
                initVideoTable();
            }
            else if (target == '#credit') {
                // $("#listcoupons").click();
                initCouponTable();
            }
            else if (target == '#reports') {
                $("#daily_stats").addClass("report-active");
                $("#daily_stats_report").show();
                getDailyStats();
            }
        }
    });
});