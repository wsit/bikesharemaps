$(document).ready(function () {
    editprofile($('#userid').val());
    $('#profile').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            smscode: {
                validators: {
                    callback: {
                        message: _sms_code,
                        callback: function (value, validator) {
                            smscode = $("#smscode").val();
                            smscode = smscode.replace(/ /g, "");
                            return true;
                            // if (smscode.search('[a-zA-Z]{2}[0-9]{6}') == 0) return true;
                            // else return false;
                        }
                    },
                    notEmpty: {
                        message: _enter_sms_code
                    }
                }
            },
            fullname: {
                validators: {
                    notEmpty: {
                        message: _enter_names
                    }
                }
            },
            mailingaddress: {
                validators: {
                    notEmpty: {
                        message: 'Enter mailing address'
                    }
                }
            },
            userage: {
                validators: {
                    notEmpty: {
                        message: 'Enter Age'
                    }
                }
            },
            usergender: {
                validators: {
                    notEmpty: {
                        message: 'Enter Gender'
                    }
                }
            },
            userrace: {
                validators: {
                    notEmpty: {
                        message: 'Enter Race'
                    }
                }
            },
            physicaladdress: {
                validators: {
                    notEmpty: {
                        message: 'Enter physical address'
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: 'Enter city'
                    }
                }
            },
            state: {
                validators: {
                    notEmpty: {
                        message: 'Enter state'
                    }
                }
            },
            zipcode: {
                validators: {
                    notEmpty: {
                        message: 'Enter zipcode'
                    }
                }
            },
            useremail: {
                validators: {
                    emailAddress: {
                        message: _email_incorrect
                    },
                    notEmpty: {
                        message: _enter_email
                    }
                }
            },
        }
    }).on('success.form.bv', function (e) {
        e.preventDefault();
        saveprofile();
    });
    $("#closeprofile").click(function () {
        window.top.close();
    });
});

//profile editing
function editprofile(userid) {
    if (userid != undefined && userid != '') {
        $.ajax({
            url: "command.php?action=editprofile&edituserid=" + userid
        }).done(function (jsonresponse) {
            jsonobject = $.parseJSON(jsonresponse);
            if (jsonobject) {
                $('#userid').val(jsonobject["userid"]);
                $('#username').val(jsonobject["username"]);
                $('#useremail').val(jsonobject["email"]);
                $('#userage').val(jsonobject["age"]);
                $('#usergender').val(jsonobject["gender"]);
                $('#userrace').val(jsonobject["race"]);
                $('#mailingaddress').val(jsonobject["mailingaddress"]);
                $('#physicaladdress').val(jsonobject["physicaladdress"]);
                $('#city').val(jsonobject["city"]);
                $('#state').val(jsonobject["state"]);
                $('#zipcode').val(jsonobject["zipcode"]);
                $('#phonenumber').val(jsonobject["phone"]);
                $('#profile').show();
            }
        });
    }
}

function saveprofile() {
    $.ajax({
        url: "command.php?action=saveprofile",
        method: "POST",
        data: {
            edituserid: $('#userid').val(),
            username: $('#username').val(),
            email: $('#useremail').val(),
            age: $('#userage').val(),
            gender: $('#usergender').val(),
            race: $('#userrace').val(),
            mailingaddress: $('#mailingaddress').val(),
            physicaladdress: $('#physicaladdress').val(),
            city: $('#city').val(),
            state: $('#state').val(),
            zipcode: $('#zipcode').val()
        }
    }).done(function (jsonresponse) {
        jsonobject = $.parseJSON(jsonresponse);
        var $target = $('html,body');
        $target.animate({scrollTop: 0}, 500);
        $('#messageboard').html('<div class="alert alert-success" role="alert">' + jsonobject.content + '</div>').fadeOut(5000);
    });
}
