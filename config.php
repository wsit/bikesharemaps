<?php
error_reporting(0);
define('DEBUG',TRUE); // debug mode, TRUE to turn on
define('ERROR',0);

$systemname="Bikesharemaps";
// $systemURL="https://www.bikesharemaps.com/"; // www.bikesharemaps.com/";
$systemURL="http://localhost/bikesharemaps/"; // For Local";
//$systemURL="http://bikesharemaps.test/"; // For Local";
//$systemURL="http://163.53.151.2/bikesharemaps/"; // For Local";
//$systemURL="http://34.207.183.227/"; // For Local";
//$systemURL="http://192.168.1.12/bikesharemaps/"; // For Local";
$systemlang="en_EN"; // language code such as en_EN, de_DE etc. - translation must be in languages/ directory, defaults to English if non-existent
$systemlat="37.9406"; // default map center point - latitude
$systemlong="-101.2549"; // default map center point - longitude
$systemzoom="15"; // default map zoom
// $systemrules="http://www.bikesharemaps.com/doc/Terms_and_conditions.txt"; // example.com/rules.htm";
$systemrules="doc/Terms_and_conditions.txt"; // example.com/rules.htm";
//$systememail="accounts@bikesharemaps.com"; // system From: email address for sending emails
$systememail="workspaceinfotech@gmail.com"; // system From: email address for sending emails
$forcestack="0"; // 0 = allow renting any bike at stand, 1 = allow renting last bicycle returned only (top of stack)
$watches["email"]=""; // notification email for notifications such as notes etc., blank if notifications not required
$watches["stack"]="1"; // 0 - do not watch stack, 1 - notify if other than the top of the stack bike is rented from a stand (independent from forcestack)
$watches["longrental"]="24"; // in hours (bike rented for more than X h)
$watches["timetoomany"]="1"; // in hours (high number of rentals by one person in a short time)
$watches["numbertoomany"]="1"; // if userlimit+numbertooomany reached in timetoomany, then notify
$watches["freetime"]="30"; // in minutes (rental changes from free to paid after this time and $credit["rent"] is deducted)
$watches["flatpricecycle"]="60"; // in minutes (uses flat price $credit["rent"] every $watches["flatpricecycle"] minutes after first paid period, i.e. $watches["freetime"]*2)
$watches["doublepricecycle"]="60"; // in minutes (doubles the rental price $credit["rent"] every $watches["doublepricecycle"] minutes after first paid period, i.e. $watches["freetime"]*2)
$watches["doublepricecyclecap"]="3"; // number of cycles after doubling of rental price $credit["rent"] is capped and stays flat (but reached cycle multiplier still applies)

$limits["registration"]="1"; // number of bikes user can rent after he registered: 0 = no bike, 1 = 1 bike etc.
$limits["increase"]="0"; // allow more bike rentals in addition to user limit: 0 = not allowed, otherwise: temporary limit increase - number of bikes

$credit["enabled"]="1"; // 0 = no credit system, 1 = apply credit system rules and deductions
$credit["currency"]="$"; // currency used for credit system
$credit["init"]="20"; // initial credit after registration
$credit["min"]="1"; // minimum credit required to allow any bike operations
$credit["rent"]="1"; // rental fee (after $watches["freetime"])
$credit["pricecycle"]="0"; // 0 = disabled, 1 = charge flat price $credit["rent"] every $watches["flatpricecycle"] minutes, 2 = charge doubled price $credit["rent"] every $watches["doublepricecycle"] minutes
$credit["longrental"]="0"; // long rental fee ($watches["longrental"] time)
$credit["limitincrease"]="10"; // credit needed to temporarily increase limit, applicable only when $limits["increase"]>0
$credit["violation"]="5"; // credit deduction for rule violations (applied by admins)

$notifyuser="0"; // 0 = no notication send to users (when admins get notified), 1 = notification messages sent to users as well

/*** Database Local ***/
$dbserver="localhost";
$dbuser="root";
$dbpassword="1";
$dbname="bikeshare";

/*** Database RDS ***/
//$dbserver="mthawk.cb0dtkwawfxc.us-east-1.rds.amazonaws.com";
//$dbuser="wsit";
//$dbpassword="wsit97480";
//$dbname="bikeshare";


/*** timezone ***/
$timezone["local"]="Europe/London";
$timezone["server"]="Europe/London";
date_default_timezone_set($timezone["server"]);

/*** Email ***/
//$email["smtp"]="mail.bikesharemaps.com"; // SMTP mail server for notifications
//$email["user"]="accounts@bikesharemaps.com"; // mail server username
//$email["pass"]="wsit97480mahedi"; // mail server password

/*** Gmail Email ***/
$email["smtp"]="ssl://smtp.gmail.com"; // SMTP mail server for notifications
$email["user"]="workspaceinfotech@gmail.com"; // mail server username
$email["pass"]="workspaceit"; // mail server password

/*** SMS related ***/
$connectors["sms"]="twilio"; // API connector used for SMS operations (connectors/ directory); empty to disable SMS system, "loopback" to simulate dummy gateway API for testing
$countrycode="1"; // international dialing code (country code prefix), no plus sign

/*** geoJSON files - uncomment line below to use, any number of geoJSON files can be included ***/
// $geojson[]="http://example.com/poi.json"; // example geojson file with points of interests to be displayed on the map

// Reports date variables

$bikeReports['today'] = date("Y-m-d");
$bikeReports['last_week'] = date("Y-m-d", strtotime("-1 week"));
$bikeReports['date_range'] = date("F d, Y", strtotime("-1 week"))." - ".date("F d, Y");
?>