<?php
$router = array(
    'users-list' => 'users-list',
    'users-delete' => 'users-delete',
    'video-list' => 'video-list',
    'video-delete' => 'video-delete',
    'inquiry-list' => 'inquiry-list',
    'help-list' => 'help-list',
    'inquiry-delete' => 'inquiry-delete',
    'stands-list' => 'stands-list',
    'stands-delete' => 'stands-delete',
    'bikes-list' => 'bikes-list',
    'bikes-delete' => 'bikes-delete',
    'get-stats' => 'get-stats',
    'daily-stats' => 'daily-stats',
    'inquiry-status' => 'inquiry-status',
    'coupon-list' => 'coupon-list'
);